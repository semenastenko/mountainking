using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MountainKing.Enviroment;
using MountainKing.GameLogic;
using MountainKing.GameInput;
using MountainKing.UI;
using PoolCore;

namespace MountainKing
{
    public class Installer : MonoBehaviour
    {
        [SerializeField] Stairstep stairstepPrefab;
        [SerializeField] Enemy[] enemyPrefabs;
        [SerializeField] float visibleOffset;

        private UIController uiController;
        private SaveLoadController saveLoadController;

        private void Awake()
        {
            var player = FindObjectOfType<Player>();
            var input = new InputController();
            var playerController = new PlayerController(player, input);
            var cameraController = new CameraController(Camera.main, player.transform, 10);

            var leaderboardController = new LeaderboardController();

            uiController = new UIController(leaderboardController);

            var poolContainer = new PoolContainer();
            poolContainer.CreatePool<Stairstep, Stairstep.Pool>(new Stairstep.Factory(stairstepPrefab), (int)visibleOffset * 2);
            foreach (var prefab in enemyPrefabs)
            {
                poolContainer.CreatePool<Enemy, Enemy.Pool>(new Enemy.Factory(prefab), 5);
            }

            var stairController = new StairController(poolContainer, player.transform, visibleOffset);
            
            var enemySpawnController = new EnemySpawnController(poolContainer, player.transform, visibleOffset);
            
            var gameController = new GameController(playerController, cameraController, stairController, enemySpawnController, leaderboardController, uiController);

            saveLoadController = new SaveLoadController(leaderboardController);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                uiController.Back();
            }
        }

        private void OnApplicationFocus(bool focus)
        {
            if (!focus)
            {
                uiController.Pause();
                saveLoadController.SaveData();
            }
        }
    }
}
