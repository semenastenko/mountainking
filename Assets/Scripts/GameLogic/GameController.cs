﻿using MountainKing.UI;
using MountainKing.Enviroment;
using UICore;
using UnityEngine;

namespace MountainKing.GameLogic
{
    public class GameController
    {
        private PlayerController _playerController;
        private CameraController _cameraController;
        private StairController _stairController;
        private EnemySpawnController _enemySpawnController;
        private LeaderboardController _leaderboardController;
        private UIController _uiController;

        private bool isDying;

        public GameController(
            PlayerController playerController,
            CameraController cameraController,
            StairController stairController,
            EnemySpawnController enemySpawnController,
            LeaderboardController leaderboardController,
            UIController uiController)
        {
            _playerController = playerController;
            _cameraController = cameraController;
            _stairController = stairController;
            _enemySpawnController = enemySpawnController;
            _leaderboardController = leaderboardController;
            _uiController = uiController;

            _playerController.OnDying += PlayerController_OnDie;
            _playerController.OnMove += PlayerController_OnMove;
            _uiController.OnButtonAction += UIController_OnButtonAction;
        }

        private void PlayerController_OnMove(Direction obj)
        {
            if (obj == Direction.up)
            {
                _leaderboardController.AddScore();
            }
        }

        private void PlayerController_OnDie(bool value)
        {
            isDying = value;
            _uiController.SetLockPause(value);
            if (value)
                _cameraController.Dispose();
            else
                GameOver();
        }

        private void UIController_OnButtonAction(object sender, ButtonEventArgs e)
        {
            if (isDying) return;
            if (e.button is PlayButton && e.button.RootPanel is MainMenuPanel)
            {
                Debug.Log($"Start Game");
                StartGame();
            }

            if (e.button is PauseButton && e.button.RootPanel is GamePanel)
            {
                Debug.Log($"Pause Game");
                _playerController.Pause(true);
                Time.timeScale = 0;
            }

            if (e.button is PlayButton && e.button.RootPanel is PausePanel)
            {
                Debug.Log($"Continue Game");
                Time.timeScale = 1;
                _playerController.Pause(false);
            }

            if (e.button is BackButton && (e.button.RootPanel is PausePanel || e.button.RootPanel is GameOverPanel))
            {
                Debug.Log($"Exit to Menu");
                Time.timeScale = 1;
                _playerController.Restart();
                _stairController.Dispose();
                _enemySpawnController.Dispose();
            }

            if (e.button is PlayButton && e.button.RootPanel is GameOverPanel)
            {
                Debug.Log($"Restart Game");
                Time.timeScale = 1;
                _playerController.Restart();
                StartGame();

            }
        }

        private void StartGame()
        {
            _playerController.StartPlayer();
            _cameraController.Start();
            _stairController.Start();
            _enemySpawnController.Start();
            _leaderboardController.ResetScore();
        }

        private void GameOver()
        {
            _uiController.GameOver();
            _stairController.Dispose();
            _enemySpawnController.Dispose();
        }
    }
}
