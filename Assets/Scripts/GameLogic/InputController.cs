﻿using UnityEngine;
using UniRx;
using System;
using UnityEngine.EventSystems;

namespace MountainKing.GameInput
{
    public class InputController : IDisposable
    {
        private IDisposable updateDispose;

        private Vector2 beginTouchPosition;
        private Vector2 touchPosition;
        
        public readonly bool DetectSwipeOnlyAfterRelease;
        public readonly float SwipeThreshold;

        public event Action<Direction> OnSwipe;
        public event Action OnTouch;

        public InputController(bool detectSwipeOnlyAfterRelease = true, float swipeThreshold = 20f)
        {
            DetectSwipeOnlyAfterRelease = detectSwipeOnlyAfterRelease;
            SwipeThreshold = swipeThreshold;
            updateDispose = Observable.EveryUpdate().Subscribe(_ => Update());
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                beginTouchPosition = Input.mousePosition;
                touchPosition = Input.mousePosition;
            }
            if (Input.GetMouseButton(0))
            {
                if (!DetectSwipeOnlyAfterRelease)
                {
                    touchPosition = Input.mousePosition;
                    CheckSwipe();
                }
            }
            if (Input.GetMouseButtonUp(0))
            {
                touchPosition = Input.mousePosition;
                CheckSwipe();
            }
        }

        private void CheckSwipe()
        {
            //Check if Vertical swipe
            if (verticalMove() > SwipeThreshold && verticalMove() > horizontalValMove())
            {
                if (touchPosition.y - beginTouchPosition.y > 0)//up swipe
                {
                    OnSwipe?.Invoke(Direction.up);
                }
                else if (touchPosition.y - beginTouchPosition.y < 0)//Down swipe
                {
                    OnSwipe?.Invoke(Direction.down);
                }
                beginTouchPosition = touchPosition;
            }  
            //Check if Horizontal swipe
            else if (horizontalValMove() > SwipeThreshold && horizontalValMove() > verticalMove())
            {
                if (touchPosition.x - beginTouchPosition.x > 0)//Right swipe
                {
                    OnSwipe?.Invoke(Direction.right);
                }
                else if (touchPosition.x - beginTouchPosition.x < 0)//Left swipe
                {
                    OnSwipe?.Invoke(Direction.left);
                }
                beginTouchPosition = touchPosition;
            }
            //On Touch
            else
            {
                OnTouch?.Invoke();
            }
        }

        float verticalMove() => Mathf.Abs(touchPosition.y - beginTouchPosition.y);

        float horizontalValMove() => Mathf.Abs(touchPosition.x - beginTouchPosition.x);

        public void Dispose()
        {
            updateDispose?.Dispose();
        }
    }
}
