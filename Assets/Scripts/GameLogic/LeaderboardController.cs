using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace MountainKing.GameLogic
{
    [Serializable]
    public class Leaderboard : ISaveable
    {
        public Leader[] leaders;

        public void UpdateData(object data)
        {
            var obj = data as Leaderboard;
            leaders = obj.leaders;
        }
    }

    [Serializable]
    public class Leader
    {
        public string name;
        public int score;
    }

    public class LeaderboardController : SaveLoad<Leaderboard>
    {
        private List<Leader> leaderboard;
        private int currentScore = 0;
        public Leader[] Leaderboard => leaderboard.ToArray();
        public int Score => currentScore;

        public event Action<int> OnUpdateScore;

        public void AddLeader(string name)
        {
            if (string.IsNullOrEmpty(name)) return;

            var existingLeader = leaderboard.Find(x => x.name == name);
            if (existingLeader != null)
            {
                if (existingLeader.score < currentScore)
                    existingLeader.score = currentScore;
            }
            else
            {
                leaderboard.Add(new Leader() { name = name, score = currentScore });
            }
        }

        public void AddScore()
        {
            currentScore++;
            OnUpdateScore?.Invoke(currentScore);
        }
        public void ResetScore()
        {
            currentScore = 0;
            OnUpdateScore?.Invoke(currentScore);
        }

        protected override Leaderboard OnSave()
        {
            return new Leaderboard() { leaders = Leaderboard };
        }

        protected override void OnLoad(Leaderboard data)
        {
            if (data != null)
                leaderboard = data.leaders.ToList();
            else
                leaderboard = new List<Leader>();
        }
    }
}
