﻿using UnityEngine;
using System;
using Cysharp.Threading.Tasks;
using LerpUtility;
using System.Threading;

namespace MountainKing.GameLogic
{
    public class MovementController : IDisposable
    {
        private CancellationTokenSource cancellationToken;
        private Transform _targetTransform;
        
        public bool IsMoving { get; private set; }
        public Vector3 TargetPosition => _targetTransform.position;

        public event Action<bool> OnFall;

        public MovementController(Transform targetTransform)
        {
            _targetTransform = targetTransform;

            cancellationToken = new CancellationTokenSource();
        }

        public async UniTask JumpMovePosition(Vector3 targetPosition, float height, float time, EaseType ease)
        {
            IsMoving = true;
            var tasks = new UniTask[]
            {
                JumpLerp(targetPosition.y + height, targetPosition.y, time, ease),
                Lerp.LerpValue(TargetPosition.x, targetPosition.x, time, ease)
                    .OnNext(x => SetPosition(new Vector3(x, TargetPosition.y, TargetPosition.z)))
                    .Task(cancellationToken),
                Lerp.LerpValue(TargetPosition.z, targetPosition.z, time, ease)
                    .OnNext(z => SetPosition(new Vector3(TargetPosition.x, TargetPosition.y, z)))
                    .Task(cancellationToken)
            };
            await UniTask.WhenAll(tasks);
            IsMoving = false;
        }

        public async UniTask JumpMoveDirection(Vector3 direction, float height, float time, EaseType ease)
        {
            IsMoving = true;
            var tasks = new UniTask[]
            {
                JumpLerp(TargetPosition.y + height, TargetPosition.y + direction.y, time, ease),
                Lerp.LerpValue(TargetPosition.x, TargetPosition.x + direction.x, time, ease)
                    .OnNext(x => SetPosition(new Vector3(x, TargetPosition.y, TargetPosition.z)))
                    .Task(cancellationToken),
                Lerp.LerpValue(TargetPosition.z, TargetPosition.z + direction.z, time, ease)
                    .OnNext(z => SetPosition(new Vector3(TargetPosition.x, TargetPosition.y, z)))
                    .Task(cancellationToken)
            };
            await UniTask.WhenAll(tasks);
            IsMoving = false;
        }

        public async UniTask Falling(float deltaPos, float time, EaseType ease)
        {
            IsMoving = true;
            OnFall?.Invoke(true);

            await Lerp.LerpValue(TargetPosition.y, TargetPosition.y - deltaPos, time, ease)
                .OnNext(y => SetPosition(new Vector3(TargetPosition.x, y, TargetPosition.z)))
                .Task(cancellationToken);

            IsMoving = false;
            OnFall?.Invoke(false);
        }

        public void SetPosition(Vector3 pos) => _targetTransform.position = pos;

        private async UniTask JumpLerp(float heightPos, float endPos, float time, EaseType ease)
        {
            await Lerp.LerpValue(TargetPosition.y, heightPos, time / 2, ease)
                .OnNext(y => SetPosition(new Vector3(TargetPosition.x, y, TargetPosition.z)))
                .Task(cancellationToken);
            await Lerp.LerpValue(TargetPosition.y, endPos, time / 2, ease)
                .OnNext(y => SetPosition(new Vector3(TargetPosition.x, y, TargetPosition.z)))
                .Task(cancellationToken);
        }

        public void Dispose()
        {
            cancellationToken.Cancel();
            cancellationToken = new CancellationTokenSource();
            IsMoving = false;
        }
    }
}
