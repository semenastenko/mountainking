﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Utility;

namespace MountainKing.GameLogic
{
    [Serializable]
    public class Save
    {
        public List<ISaveable> data = new List<ISaveable>();
    }

    public class SaveLoadController
    {
        private ISaveLoad[] _saveLoads;
        private Save save;

        public SaveLoadController(params ISaveLoad[] saveLoads)
        {
            _saveLoads = saveLoads;
            LoadData();
        }

        public void SaveData()
        {
            foreach (var item in _saveLoads)
            {
                var saveable = save.data.Find(x => x.GetType() == item.SaveableType());
                if (saveable != null)
                    saveable.UpdateData(item.OnSave());
                else
                    save.data.Add(item.OnSave());



            }
            var data = BinaryFormatterUtility.ToBinary(save);
            PlayerPrefs.SetString("save", data);
        }

        private void LoadData()
        {
            var data = PlayerPrefs.GetString("save");
            if (string.IsNullOrEmpty(data))
            {
                save = new Save();
            }
            else
            {
                save = BinaryFormatterUtility.FromBinary<Save>(data);
            }

            foreach (var item in _saveLoads)
            {
                var saveable = save.data.Find(x => x.GetType() == item.SaveableType());
                item.OnLoad(saveable);
            }
        }

        private void ClearData()
        {
            save.data = new List<ISaveable>();
            var data = BinaryFormatterUtility.ToBinary(save);
            PlayerPrefs.SetString("save", data);
            LoadData();
        }
    }
}
