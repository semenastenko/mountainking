﻿using System;

namespace PoolCore
{
    /// <summary>
    /// Интерфейс для реализации пула объектов
    /// </summary>
    public interface IPool
    {
        int TotalCount { get; }
        int InactiveCount { get; }
        int ActiveCount { get; }
        Type ItemType { get; }
        bool Despawn(object obj);
        object Spawn();
        bool TrySpawn(ref object obj);
    }
}