﻿namespace PoolCore
{
    /// <summary>
    /// Интерфейс для реализации фабрики создания экземпляра объекта.
    /// </summary>
    public interface IFactory<Item>
    {
        Item Create();
    }
}