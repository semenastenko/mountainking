﻿using UnityEngine;

namespace PoolCore
{
    /// <summary>
    /// Базовая реализация пула MonoBehaviour объектов.
    /// </summary>
    public class ObjectPool<Poolable> : PoolBase<Poolable>
        where Poolable : Component
    {
        Transform _originalParent;

        protected override void OnCreated(Poolable item)
        {
            item.gameObject.SetActive(false);
            _originalParent = item.transform.parent;
        }

        protected override void OnDestroyed(Poolable item)
        {
            GameObject.Destroy(item.gameObject);
        }

        protected override void OnSpawned(Poolable item, IParamtable param)
        {
            item.gameObject.SetActive(true);
        }

        protected override void OnDespawned(Poolable item)
        {
            item.gameObject.SetActive(false); 

            if (item.transform.parent != _originalParent)
            {
                item.transform.SetParent(_originalParent, false);
            }
        }
    }
}
