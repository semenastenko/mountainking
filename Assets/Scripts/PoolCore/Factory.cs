﻿using System;
using UnityEngine;

namespace PoolCore
{
    /// <summary>
    /// Базовая реализация фабрики.
    /// </summary>
    public class Factory<Item> : IFactory<Item>
    {
        Item _reference;

        public Factory(Item item)
        {
            _reference = item;
        }
        public Item Create()
        {
            return OnCreate(_reference);
        }

        protected virtual Item OnCreate(Item item)
        {
            throw new NotImplementedException();
        }
    }
}