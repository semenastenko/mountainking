﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace PoolCore
{
    /// <summary>
    /// Базовая реализация пула объектов.
    /// </summary>
    public class PoolBase<Poolable> : IPool
    {
        private Stack<Poolable> _inactiveItems;
        private IFactory<Poolable> _factory;
        private int _poolSize;
        private int _activeCount;

        public IEnumerable<Poolable> InactiveItems => _inactiveItems;
        public int TotalCount => InactiveCount + ActiveCount;
        public int InactiveCount => _inactiveItems.Count;
        public int ActiveCount => _activeCount;
        public Type ItemType => typeof(Poolable);

        public static Pool Create<Pool>(IFactory<Poolable> factory, int poolSize)
            where Pool : PoolBase<Poolable>, new()
        {
            var pool = new Pool();
            (pool as PoolBase<Poolable>).Construct(factory, poolSize);
            return pool;
        }

        private void Construct(IFactory<Poolable> factory, int poolSize)
        {
            _factory = factory;
            _poolSize = poolSize;
            _inactiveItems = new Stack<Poolable>(_poolSize);

            for (int i = 0; i < _poolSize; i++)
            {
                _inactiveItems.Push(AddNew());
            }
        }

        private Poolable AddNew()
        {
            var item = _factory.Create();
            OnCreated(item);
            return item;
        }

        bool IPool.Despawn(object item)
        {
            return Despawn((Poolable)item);
        }

        public bool Despawn(Poolable item)
        {
            if (_inactiveItems.Contains(item)) return false;

            _activeCount--;
            _inactiveItems.Push(item);

            OnDespawned(item);

            return true;
        }

        object IPool.Spawn()
        {
            return Spawn(default(IParamtable));
        }

        public Poolable Spawn(IParamtable param)
        {
            if (_inactiveItems.Count == 0) throw new Exception("Pool active count out of range");
            var item = _inactiveItems.Pop();
            _activeCount++;
            OnSpawned(item, param);

            return item;
        }

        bool IPool.TrySpawn(ref object obj)
        {
            Poolable poolable = (Poolable)obj;
            return TrySpawn(ref poolable, default(IParamtable));
        }

        public bool TrySpawn(ref Poolable obj, IParamtable param)
        {
            try
            {
                obj = Spawn(param);
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected virtual void OnCreated(Poolable item) { }
        protected virtual void OnDespawned(Poolable item) { }
        protected virtual void OnSpawned(Poolable item, IParamtable param) { }
        protected virtual void OnDestroyed(Poolable item) { }
    }
}
