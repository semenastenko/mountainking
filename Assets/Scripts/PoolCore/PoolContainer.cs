﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace PoolCore
{
    /// <summary>
    /// Класс, хранящий список созданных пулов.
    /// </summary>
    public class PoolContainer
    {
        private readonly List<IPool> pools = new List<IPool>();

        public IPool[] Pools => pools.ToArray();

        /// <summary>
        /// Создание пула и добавление его в список всех пулов.
        /// </summary>
        public IPool CreatePool<Poolable, Pool>(Factory<Poolable> factory, int size)
            where Pool : PoolBase<Poolable>, new()
        {
            var pool = PoolBase<Poolable>.Create<Pool>(factory, size);

            pools.Add(pool);

            return pool;
        }

        /// <summary>
        /// Получение указанных пулов из списка всех пулов.
        /// </summary>
        public List<Pool> GetPools<Pool>() where Pool : IPool
        {
            return pools.FindAll(x => x is Pool).Cast<Pool>().ToList();
        }
    }
}
