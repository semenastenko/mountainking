﻿namespace PoolCore
{
    /// <summary>
    /// Интерфейс для передачи параметров в пул объекта.
    /// </summary>
    public interface IParamtable
    {
        object[] GetAllParams();
    }
}