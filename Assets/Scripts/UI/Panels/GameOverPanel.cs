﻿using Cysharp.Threading.Tasks;
using UICore;
using LerpUtility;
using UnityEngine;
using UnityEngine.UI;
using MountainKing.GameLogic;

namespace MountainKing.UI
{
    public class GameOverPanel : UIPanel
    {
        private LeaderboardController _lbController;
        private InputField _inputField;

        private void Awake()
        {
            _inputField = GetComponentInChildren<InputField>();
        }

        protected override void BeginOpen()
        {
            var username = PlayerPrefs.GetString("CurrentUser");

            if (!string.IsNullOrEmpty(username))
                _inputField.text = username;
        }

        protected override void BeginClose()
        {
            if (!string.IsNullOrEmpty(_inputField.text))
            {
                PlayerPrefs.SetString("CurrentUser", _inputField.text);
                _lbController.AddLeader(_inputField.text);
            }
        }

        protected async override UniTask Opening()
        {
            await Lerp.LerpValue(-Screen.height / 2, transform.position.y, 0.2f, EaseType.QuinticOut)
                .OnNext(y => transform.position = new Vector3(transform.position.x, y, transform.position.z))
                .Task();
        }

        protected async override UniTask Closing()
        {
            await Lerp.LerpValue(transform.position.y, Screen.height * 1.5f, 0.2f, EaseType.Linear)
                .OnNext(y => transform.position = new Vector3(transform.position.x, y, transform.position.z))
                .Task();
        }

        public void SetLeaderBoardController(LeaderboardController lbController)
        {
            _lbController = lbController;
        }
    }
}
