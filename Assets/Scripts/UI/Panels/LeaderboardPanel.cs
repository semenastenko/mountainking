﻿using Cysharp.Threading.Tasks;
using LerpUtility;
using MountainKing.GameLogic;
using System.Collections.Generic;
using System.Linq;
using UICore;
using UnityEngine;
using UnityEngine.UI;

namespace MountainKing.UI
{
    public class LeaderboardPanel : UIPanel
    {
        [SerializeField] LeaderElement prefab;
        [SerializeField] float elementOffset = 0.5f;
        private LeaderboardController _lbController;
        private ScrollRect _scrollRect;

        private List<LeaderElement> elements = new List<LeaderElement>();

        private void Awake()
        {
            _scrollRect = GetComponentInChildren<ScrollRect>();
        }

        protected override void BeginOpen()
        {
            UpdateLeaderboard();
        }

        protected async override UniTask Opening()
        {
            await Lerp.LerpValue(-Screen.height / 2, transform.position.y, 0.2f, EaseType.BackOut)
                .OnNext(y => transform.position = new Vector3(transform.position.x, y, transform.position.z))
                .Task();
        }
        protected async override UniTask Closing()
        {
            await Lerp.LerpValue(transform.position.y, Screen.height * 1.5f, 0.2f, EaseType.BackIn)
                .OnNext(y => transform.position = new Vector3(transform.position.x, y, transform.position.z))
                .Task();
        }

        public void SetLeaderBoardController(LeaderboardController lbController)
        {
            _lbController = lbController;
        }

        private void UpdateLeaderboard()
        {
            var liderboard = _lbController.Leaderboard.ToList();
            liderboard.Sort((x1, x2) => x1.score.CompareTo(x2.score));
            liderboard.Reverse();

            for (int i = 0; i < liderboard.Count; i++)
            {
                var leader = liderboard[i];
                if (elements.Count <= i)
                {
                    AddElement();
                }
                elements[i].SetText(leader.name, leader.score, i + 1);
            }
        }

        private void AddElement()
        {
            var position = Vector2.zero;
            if (elements.Count > 0)
            {
                position = elements[elements.Count - 1].rectTransform.anchoredPosition;
                position.y -= elementOffset;
            }
            var newElement = Instantiate(prefab, _scrollRect.content);
            newElement.rectTransform.anchoredPosition = new Vector2(position.x, position.y - newElement.rectTransform.sizeDelta.y / 2);
            elements.Add(newElement);
        }
    }
}
