using Cysharp.Threading.Tasks;
using LerpUtility;
using UICore;
using UnityEngine;

namespace MountainKing.UI
{
    public class MainMenuPanel : BeginingPanel
    {
        protected async override UniTask Opening()
        {
            await Lerp.LerpValue(-Screen.height / 2, transform.position.y, 0.2f, EaseType.Linear)
                .OnNext(y => transform.position = new Vector3(transform.position.x, y, transform.position.z))
                .Task();
        }
        protected async override UniTask Closing()
        {
            await Lerp.LerpValue(transform.position.y, Screen.height * 1.5f, 0.2f, EaseType.QuinticIn)
                .OnNext(y => transform.position = new Vector3(transform.position.x, y, transform.position.z))
                .Task();
        }
    }
}
