using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MountainKing.UI
{
    public class LeaderElement : MonoBehaviour
    {
        [SerializeField] Text numberText;
        [SerializeField] Text nameText;
        [SerializeField] Text scoreText;
        public RectTransform rectTransform => transform as RectTransform;

        public void SetText(string leaderName, int score, int number)
        {
            scoreText.text = score.ToString();
            nameText.text = leaderName;

            switch (number)
            {
                case 1:
                    numberText.text = $"<color=#FFD700>{number}</color>";
                    break;
                case 2:
                    numberText.text = $"<color=#C0C0C0>{number}</color>";
                    break;
                case 3:
                    numberText.text = $"<color=#B08B57>{number}</color>";
                    break;
                default:
                    numberText.text = $"<color=#4D4D4D>{number}</color>";
                    break;
            }
        }
    }
}
