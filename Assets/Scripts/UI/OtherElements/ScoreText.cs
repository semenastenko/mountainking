using System.Collections;
using System.Collections.Generic;
using UICore;
using UnityEngine;
using UnityEngine.UI;

namespace MountainKing.UI
{
    public class ScoreText : Text, IUIElement
    {
        public UIPanel RootPanel { get; private set; }
        public void Initialize(IUIElement parent)
        {
            RootPanel = parent as UIPanel;
        }
    }
}
