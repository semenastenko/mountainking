using System;
using UICore;

namespace MountainKing.UI
{
    public class PauseButton : UIButton
    {
        protected override TimeSpan GetDelay() => TimeSpan.FromSeconds(0f);
    }
}
