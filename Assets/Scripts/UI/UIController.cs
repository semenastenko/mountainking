﻿using System.Collections.Generic;
using UnityEngine;
using UICore;
using Cysharp.Threading.Tasks;
using MountainKing.GameLogic;
using UnityEngine.UI;

namespace MountainKing.UI
{
    public class UIController : UIContainer
    {
        private List<(string, bool)> bindLog = new List<(string, bool)>();

        public UIController(LeaderboardController leaderboardController)
        : base()
        {
            PanelManager.GetElement<LeaderboardPanel>(x => x is LeaderboardPanel)
                .SetLeaderBoardController(leaderboardController);

            PanelManager.GetElement<GameOverPanel>(x => x is GameOverPanel)
                .SetLeaderBoardController(leaderboardController);

            var scoreText = PanelManager.GetElement<GamePanel>(x => x is GamePanel)
                .GetComponentInChildren<ScoreText>();

            leaderboardController.OnUpdateScore += (score) => scoreText.text = score.ToString();

            BindButtons();
        }

        private void BindButtons()
        {
            //Bind PlayButton in MainMenu
            bindLog.Add(BindQueue<PlayButton>(x => x.RootPanel is MainMenuPanel,
                ButtonAction,
                _ => PanelAction<MainMenuPanel>(x => x is MainMenuPanel, PanelState.closed),
                _ => PanelAction<GamePanel>(x => x is GamePanel, PanelState.open)));

            //Bind PauseButton in Game
            bindLog.Add(BindQueue<PauseButton>(x => x.RootPanel is GamePanel,
                _ => PanelAction<PausePanel>(x => x.Parent is GamePanel, PanelState.open),
                ButtonAction));

            //Bind ContinueButton in Pause
            bindLog.Add(BindQueue<PlayButton>(x => x.RootPanel is PausePanel,
                ButtonAction,
                _ => PanelAction<PausePanel>(x => x.Parent is GamePanel, PanelState.closed)
                ));

            //Bind QuitButton in Pause
            bindLog.Add(BindQueue<BackButton>(x => x.RootPanel is PausePanel,
                ButtonAction,
                _ => UniTask.WhenAll(PanelAction<GamePanel>(x => x is GamePanel, PanelState.closed),
                PanelAction<MainMenuPanel>(x => x is MainMenuPanel, PanelState.open))
                ));

            //Bind RestartButton in GameOver
            bindLog.Add(BindQueue<PlayButton>(x => x.RootPanel is GameOverPanel,
                ButtonAction,
                _ => PanelAction<GameOverPanel>(x => x.Parent is GamePanel, PanelState.closed)
                ));

            //Bind QuitButton in GameOver
            bindLog.Add(BindQueue<BackButton>(x => x.RootPanel is GameOverPanel,
                ButtonAction,
                _ => UniTask.WhenAll(PanelAction<GamePanel>(x => x is GamePanel, PanelState.closed),
                PanelAction<MainMenuPanel>(x => x is MainMenuPanel, PanelState.open))
                ));

            //Bind LeaderboardButton in MainMenu
            bindLog.Add(BindQueue<LeaderboardButton>(x => x.RootPanel is MainMenuPanel,
                _ => PanelAction<LeaderboardPanel>(x => x.Parent is MainMenuPanel, PanelState.open),
                ButtonAction));

            //Bind BackButton in LeaderboardPanel
            bindLog.Add(BindQueue<BackButton>(x => x.RootPanel is LeaderboardPanel,
                _ => PanelAction<LeaderboardPanel>(x => x.Parent is MainMenuPanel, PanelState.closed),
                ButtonAction));
        }

        public async void GameOver()
        {
            await PanelAction<GameOverPanel>(x => x.Parent is GamePanel, PanelState.open);
        }

        public async void Pause()
        {
            if (PanelManager.FrontPanel is GamePanel)
            {
                await PanelAction<PausePanel>(x => x.Parent is GamePanel, PanelState.open);
                await ButtonAction(ButtonManager.GetElement<PauseButton>(x => x.RootPanel is GamePanel));
            }
        }

        public void SetLockPause(bool value)
        {
            PanelManager.GetElement<PausePanel>(x => x.Parent is GamePanel).IsLock = value;
        }

        public async void Back()
        {
            if (PanelManager.FrontPanel is GamePanel)
            {
                Pause();
            }
            else if (PanelManager.FrontPanel is PausePanel)
            {
                await ButtonAction(ButtonManager.GetElement<PlayButton>(x => x.RootPanel is PausePanel));
                await PanelAction<PausePanel>(x => x.Parent is GamePanel, PanelState.closed);
            }
            else if (PanelManager.FrontPanel is GameOverPanel)
            {
                await ButtonAction(ButtonManager.GetElement<BackButton>(x => x.RootPanel is GameOverPanel));
                await PanelAction<GamePanel>(x => x is GamePanel, PanelState.closed);
                await PanelAction<MainMenuPanel>(x => x is MainMenuPanel, PanelState.open);
            }
            else if (PanelManager.FrontPanel is LeaderboardPanel)
            {
                await PanelAction<LeaderboardPanel>(x => x.Parent is MainMenuPanel, PanelState.closed);
            }
        }
    }
}
