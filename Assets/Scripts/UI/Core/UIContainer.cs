using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Cysharp.Threading.Tasks;

namespace UICore
{
    public abstract class UIContainer : IDisposable
    {
        UIPanel.Manager _panelManager;
        UIButton.Manager _buttonManager;

        private List<System.IDisposable> _receiveDisposes = new List<System.IDisposable>();

        public event PanelEventHandler OnPanelAction;
        public event ButtonEventHandler OnButtonAction;

        public UIPanel.Manager PanelManager => _panelManager;
        public UIButton.Manager ButtonManager => _buttonManager;

        public UIContainer()
        {
            _panelManager = UIElementManager<UIPanel>.Create<UIPanel.Manager, UIPanel>(GameObject.FindObjectsOfType<UIPanel>(true));
            _buttonManager = UIElementManager<UIButton>.Create<UIButton.Manager, UIButton>(GameObject.FindObjectsOfType<UIButton>(true)); ;
        }

        protected (string, bool) BindState<Btn, Pnl>(Predicate<Btn> buttonMatch, Predicate<Pnl> panelMatch, PanelState state, TaskAction<UIButton> action = null) where Btn : UIButton where Pnl : UIPanel
        {
            var panel = _panelManager.GetElement<Pnl>(panelMatch);
            var button = _buttonManager.GetElement<Btn>(buttonMatch);
            if (button && panel)
            {
                button.Subscribe(_ => GetPanelAction(panel, state, button));
                if (action != null) button.Subscribe(action);
                return ($"{panel.name}->{button.name}", true);
            }
            return ($"{panel?.name}->{button?.name}", false);
        }

        protected (string, bool) BindQueue<Btn>(Predicate<Btn> buttonMatch, params TaskAction<UIButton>[] actions) where Btn : UIButton
        {
            var button = _buttonManager.GetElement<Btn>(buttonMatch);
            if (button)
            {
                button.Subscribe(btn => QueueHandlers(actions, btn));
                return ($"{button.name}", true);
            }
            return ($"{button?.name}", false);
        }

        protected async UniTask QueueHandlers(TaskAction<UIButton>[] handlers, UIButton button)
        {
            foreach (var handler in handlers)
            {
                await handler(button);
            }
        }

        protected async UniTask PanelAction<Pnl>(Predicate<Pnl> panelMatch, PanelState state) where Pnl : UIPanel
        {
            var panel = _panelManager.GetElement<Pnl>(panelMatch);
            if (panel) await GetPanelAction(panel, state, this);
            OnPanelAction?.Invoke(this, new PanelEventArgs(panel));
        }

        protected UniTask GetPanelAction(UIPanel panel, PanelState state, object sender)
            => (state) switch
            {
                PanelState.open => panel.Open(sender),
                PanelState.opening => panel.Open(sender),
                PanelState.closed => panel.Close(sender),
                PanelState.closing => panel.Close(sender),
                _ => default(UniTask),
            };

        protected async UniTask ButtonAction(UIButton button)
        {
            OnButtonAction?.Invoke(this, new ButtonEventArgs(button));
            await UniTask.Yield();

        }

        public void Dispose()
        {
            foreach (var item in _receiveDisposes)
            {
                item?.Dispose();
            }
            _receiveDisposes.Clear();
        }
    }
}
