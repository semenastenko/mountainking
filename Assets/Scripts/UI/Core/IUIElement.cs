using System.Collections;
using UnityEngine;

namespace UICore
{
    public interface IUIElement
    {
        void Initialize(IUIElement parent);
    }
}