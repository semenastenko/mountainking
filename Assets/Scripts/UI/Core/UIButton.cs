using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;

namespace UICore
{
    public struct ButtonEventArgs
    {
        public UIButton button;

        public ButtonEventArgs(UIButton button)
        {
            this.button = button;
        }
    }

    public delegate void ButtonEventHandler(object sender, ButtonEventArgs e);
    public delegate UniTask TaskAction();
    public delegate UniTask TaskAction<T>(T obj);

    public class UIButton : Button, IUIElement
    {
        public UIPanel RootPanel { get; private set; }

        void IUIElement.Initialize(IUIElement parent)
        {
            RootPanel = parent as UIPanel;
        }

        public void Subscribe(TaskAction<UIButton> action) => onClick.AddListener(() => InvokeAction(action));

        private async void InvokeAction(TaskAction<UIButton> action)
        {
            Debug.Log(this.name + " invoke!");
            await UniTask.Delay(GetDelay(), ignoreTimeScale: true);
            await action.Invoke(this);
        }

        protected virtual System.TimeSpan GetDelay() => System.TimeSpan.FromSeconds(0.1f);

        public class Manager : UIElementManager<UIButton>
        {
            protected override void OnConstruct()
            {
                foreach (var button in AllElements)
                {
                    (button as IUIElement).Initialize(GetElementInParent<UIPanel>(button.transform));
                }
            }
        }
    }
}
