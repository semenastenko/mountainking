using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MountainKing.GameLogic;

namespace MountainKing.Enviroment
{
    public class CubeEnemy : Enemy
    {
        private MovementController _movementController;
        private bool canDespawn;

        [SerializeField] float moveTime = 0.6f;
        [SerializeField] float deltaPos = 1;
        [SerializeField] float stepCount = 7;

        protected override void OnCreate()
        {
            _movementController = new MovementController(transform);
        }

        protected async override void OnSpawn()
        {
            if (_movementController.IsMoving) return;

            canDespawn = false;
            var range = GetClampRange(_movementController.TargetPosition.z);
            _movementController.SetPosition(new Vector3(
                _movementController.TargetPosition.x, 
                _movementController.TargetPosition.y,
                Random.Range(range.min, range.max)));

            try
            {
                while (!canDespawn)
                {
                    range = GetClampRange(_movementController.TargetPosition.z);
                    float deltaZ = Random.Range(range.min, range.max);
                    Vector3 dir = new Vector3(-1, -1, deltaZ);
                    await _movementController.JumpMoveDirection(dir, deltaPos, moveTime, LerpUtility.EaseType.QuadraticInOut);
                }
            }
            catch
            {

            }
        }
        private (float min, float max) GetClampRange(float position) => (-stepCount * deltaPos / 2 - position, stepCount * deltaPos / 2 - position);

        protected override void OnDespawn()
        {
            _movementController.Dispose();
            canDespawn = false;
        }
    }
}
