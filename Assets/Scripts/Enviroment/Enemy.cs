using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PoolCore;
using System;

namespace MountainKing.Enviroment
{
    public abstract class Enemy : MonoBehaviour
    {
        protected abstract void OnCreate();
        protected abstract void OnSpawn();
        protected abstract void OnDespawn();

        /// <summary>
        /// ���������, ����������� ��������� IParamtable,
        /// ��� �������� ���������� � ���.
        /// </summary>
        public struct Params : IParamtable
        {
            public readonly Vector3 Position;

            public Params(Vector3 position)
            {
                Position = position;
            }

            public object[] GetAllParams() => new object[] { Position };
        }

        /// <summary>
        /// ������ "���" ��� �������� �������� ���� ��������.
        /// </summary>
        public class Pool : ObjectPool<Enemy>
        {
            protected override void OnCreated(Enemy item)
            {
                base.OnCreated(item);
                item.OnCreate();
            }

            protected override void OnSpawned(Enemy item, IParamtable param)
            {
                var currentParam = (Params)param;
                item.transform.position = currentParam.Position;

                base.OnSpawned(item, param);
                
                item.OnSpawn();
            }

            protected override void OnDespawned(Enemy item)
            {
                item.OnDespawn();
                base.OnDespawned(item);
            }
        }

        /// <summary>
        /// ������ "�������" ��� �������� �������� ���������� �������.
        /// </summary>
        public class Factory : Factory<Enemy>
        {
            private Transform _group;
            public Factory(Enemy item)
                : base(item)
            {
                _group = new GameObject($"Container {item.name}").transform;
            }

            protected override Enemy OnCreate(Enemy item)
            {
                var placeable = GameObject.Instantiate(item, _group);

                placeable.name = item.name;

                return placeable;
            }
        }
    }
}
