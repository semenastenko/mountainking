using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LerpUtility;

namespace MountainKing.Enviroment
{
    public class Player : MonoBehaviour
    {
        [SerializeField] float moveTime = 0.3f;
        [SerializeField] float deltaPos = 1;
        [SerializeField] float stepCount = 7;

        public float MoveTime => moveTime;
        public float DeltaPos => deltaPos;
        public float StepCount => stepCount;

        private Vector3 startPosition;
        private Vector3 startScale;

        public event Action<bool> OnCollision;

        private void Awake()
        {
            startPosition = Vector3.up * 0.5f;
            startScale = transform.lossyScale;
        }

        public void ReInit()
        {
            transform.position = startPosition;
            transform.localScale = startScale;
            gameObject.SetActive(true);
        }

        private void OnCollisionEnter(Collision collision)
        {
            var rb = collision.rigidbody;
            if (rb == null) return;

            var enemy = rb.GetComponent<Enemy>();
            if (enemy == null) return;

            Debug.Log($"Oncollision {collision.collider.name}");
            Die();
        }



        private async void Die()
        {
            OnCollision?.Invoke(true);
            await Lerp.LerpValue(transform.localScale.x, 0, 0.5f, EaseType.QuadraticOut)
                .OnNext(x => transform.localScale = Vector3.one * x)
                .Task();
            gameObject.SetActive(false);
            await UniTask.Delay(500);
            OnCollision?.Invoke(false);
        }
    }
}
