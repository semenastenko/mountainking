using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

namespace MountainKing.Enviroment
{
    public class CameraController : IDisposable
    {
        private IDisposable updateDispose;
        private Camera _camera;
        private Transform _targetTransform;
        private Vector3 offset;
        private Vector3 velocity;
        private float _smoothTime;
        public CameraController(Camera camera, Transform target, float smoothTime)
        {
            _camera = camera;
            _targetTransform = target;
            _smoothTime = smoothTime;

            offset = _camera.transform.position - _targetTransform.position;
        }

        public void Start()
        {
            _camera.transform.position = _targetTransform.position + offset;
            if (updateDispose == null)
                updateDispose = Observable.EveryFixedUpdate().Subscribe(_ => Update());
        }

        private void Update()
        {
            var smoothPosition = Vector3.SmoothDamp(_camera.transform.position, _targetTransform.position + offset, ref velocity, _smoothTime * Time.deltaTime);
            _camera.transform.position = new Vector3(smoothPosition.x, smoothPosition.y, _camera.transform.position.z);
        }

        public void Dispose()
        {
            updateDispose?.Dispose();
            updateDispose = null;
        }
    }
}
