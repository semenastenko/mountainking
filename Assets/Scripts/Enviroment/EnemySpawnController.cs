using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PoolCore;
using System;
using Random = UnityEngine.Random;
using UniRx;

namespace MountainKing.Enviroment
{
    public class EnemySpawnController : IDisposable
    {
        private PoolContainer _poolContainer;
        private Queue<(Enemy item, Enemy.Pool pool)> spawned;
        private Transform _targetTransform;
        private float _offset;
        private IDisposable updateDispose;
        private float currentTime;
        private float nextSpawnTime;
        private (float min, float max) nextTimeRange = (1, 6);

        public EnemySpawnController(PoolContainer poolContainer, Transform target, float offset)
        {
            _poolContainer = poolContainer;
            _targetTransform = target;
            _offset = offset;
            spawned = new Queue<(Enemy enemy, Enemy.Pool pool)>();
        }

        public void Start()
        {
            if (updateDispose == null)
                updateDispose = Observable.EveryLateUpdate().Subscribe(_ => Update());
            currentTime = 0;
            RandomNextTime();
        }

        private void Update()
        {
            if (spawned.Count > 0 && spawned.Peek().item.transform.position.x <= _targetTransform.position.x - _offset + 1)
            {
                DespawnFirst();
            }
            if (currentTime > nextSpawnTime)
            {
                currentTime = 0;
                RandomNextTime();
                TryPlace(new Vector3((int)_targetTransform.position.x + _offset, (int)_targetTransform.position.x + _offset + 0.5f, 0));
            }

            currentTime += Time.deltaTime;
        }

        private void RandomNextTime() => nextSpawnTime = Random.Range(nextTimeRange.min, nextTimeRange.max) / 3;

        private bool TryPlace(Vector3 spawnPosition)
        {
            Enemy.Pool pool = GetRandomPool() as Enemy.Pool;
            if (pool == null) return false;

            var item = pool.Spawn(new Enemy.Params(spawnPosition));

            spawned.Enqueue((item, pool));

            return true;
        }

        private IPool[] GetPools() =>
            _poolContainer.GetPools<Enemy.Pool>().ToArray();

        private IPool GetRandomPool()
        {
            var randomPool = new List<IPool>();
            foreach (var pool in GetPools())
            {
                if (pool.InactiveCount > 0)
                {
                    randomPool.Add(pool);
                }
            }
            if (randomPool.Count == 0) return null;

            int randomIndex = Random.Range(0, randomPool.Count);
            return randomPool[randomIndex];
        }

        private void DespawnFirst()
        {
            var element = spawned.Dequeue();
            element.pool.Despawn(element.item);
        }

        private void Reset()
        {
            while (spawned.Count != 0)
            {
                DespawnFirst();
            }
        }

        public void Dispose()
        {
            updateDispose?.Dispose();
            updateDispose = null;
            Reset();
        }
    }
}
