using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PoolCore;

namespace MountainKing.Enviroment
{
    public class Stairstep : MonoBehaviour
    {


        /// <summary>
        /// ���������, ����������� ��������� IParamtable,
        /// ��� �������� ���������� � ���.
        /// </summary>
        public struct Params : IParamtable
        {
            public readonly Vector3 Position;

            public Params(Vector3 position)
            {
                Position = position;
            }

            public object[] GetAllParams() => new object[] { Position };
        }

        /// <summary>
        /// ������ "���" ��� �������� �������� ���� ��������.
        /// </summary>
        public class Pool : ObjectPool<Stairstep>
        {
            protected override void OnCreated(Stairstep item)
            {
                base.OnCreated(item);
            }

            protected override void OnSpawned(Stairstep item, IParamtable param)
            {
                var currentParam = (Params)param;
                item.transform.position = currentParam.Position;

                base.OnSpawned(item, param);
            }

            protected override void OnDespawned(Stairstep item)
            {
                base.OnDespawned(item);
            }
        }

        /// <summary>
        /// ������ "�������" ��� �������� �������� ���������� �������.
        /// </summary>
        public class Factory : Factory<Stairstep>
        {
            private Transform _group;
            public Factory(Stairstep item)
                : base(item)
            {
                _group = new GameObject($"Container {item.name}").transform;
            }

            protected override Stairstep OnCreate(Stairstep item)
            {
                var placeable = GameObject.Instantiate(item, _group);

                placeable.name = item.name;

                return placeable;
            }
        }
    }
}
