using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PoolCore;
using UniRx;
using System;

namespace MountainKing.Enviroment
{
    public class StairController : IDisposable
    {
        private Stairstep.Pool pool;
        private Queue<Stairstep> spawned;
        private Stairstep lastSpawned;
        private Transform _targetTransform;
        private float _offset;
        private IDisposable updateDispose;

        public StairController(PoolContainer poolContainer, Transform target, float offset)
        {
            spawned = new Queue<Stairstep>();
            pool = poolContainer.GetPools<Stairstep.Pool>()[0];
            _targetTransform = target;
            _offset = offset;
        }

        public void Start()
        {
            if (updateDispose == null)
                updateDispose = Observable.EveryLateUpdate().Subscribe(_ => Update());
        }

        private void Update()
        {
            if (spawned.Count > 0 && spawned.Peek().transform.position.x <= _targetTransform.position.x - _offset / 2)
            {
                DespawnFirst();
            }
            if (lastSpawned != null && lastSpawned.transform.position.x < _targetTransform.position.x + _offset)
            {
                TryPlace(lastSpawned.transform.position + Vector3.up + Vector3.right);
            }
            if (lastSpawned == null)
            {
                TryPlace(_targetTransform.position * 2 + (Vector3.down + Vector3.left) * _offset + Vector3.down);
            }
        }

        private bool TryPlace(Vector3 nextPos)
        {
            Stairstep item = null;
            pool.TrySpawn(ref item, new Stairstep.Params(nextPos));
            if (item == null) return false;

            spawned.Enqueue(item);
            lastSpawned = item;
            return true;
        }

        private void DespawnFirst()
        {
            var element = spawned.Dequeue();
            pool.Despawn(element);
        }

        private void Reset()
        {
            while (spawned.Count != 0)
            {
                DespawnFirst();
            }
            lastSpawned = null;
        }

        public void Dispose()
        {
            updateDispose?.Dispose();
            updateDispose = null;
            Reset();
        }
    }
}
