using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MountainKing.GameInput;
using MountainKing.GameLogic;
using System;
using LerpUtility;

namespace MountainKing.Enviroment
{
    public class PlayerController
    {
        private Player _player;
        private InputController _inputController;
        private MovementController _movementController;

        public event Action<bool> OnDying;
        public event Action<Direction> OnMove;

        public Vector3 currentPosition;

        public PlayerController(Player player, InputController inputController)
        {
            _player = player;
            _inputController = inputController;
            _movementController = new MovementController(_player.transform);
        }

        public void StartPlayer()
        {
            _player.ReInit();
            currentPosition = _player.transform.position;
            _inputController.OnSwipe += InputController_OnSwipe;
            _inputController.OnTouch += InputController_OnTouch;
            _movementController.OnFall += Die;
            _player.OnCollision += Die;

        }

        public void Pause(bool value)
        {
            if (value)
            {
                _inputController.OnSwipe -= InputController_OnSwipe;
                _inputController.OnTouch -= InputController_OnTouch;
            }
            else
            {
                _inputController.OnSwipe += InputController_OnSwipe;
                _inputController.OnTouch += InputController_OnTouch;
            }
        }

        public void Restart()
        {
            Dispose();
            _movementController.OnFall -= Die;
            _player.OnCollision -= Die;
            _movementController.Dispose();
            _player.ReInit();
        }

        private void InputController_OnTouch() =>
             Move(Direction.up);

        private void InputController_OnSwipe(Direction swipe)
        {
            switch (swipe)
            {
                case Direction.left:
                case Direction.right:
                    Move(swipe);
                    break;
            }
        }

        private async void Move(Direction direction)
        {
            if (_movementController.IsMoving) _movementController.Dispose();

            OnMove?.Invoke(direction);
            try
            {
                switch (direction)
                {
                    case Direction.up:
                        currentPosition += Vector3.up + Vector3.right;
                        await _movementController.JumpMovePosition(currentPosition,
                            _player.DeltaPos * 0.5f, _player.MoveTime, EaseType.QuadraticInOut);
                        break;
                    case Direction.left:
                        currentPosition += Vector3.forward;
                        await _movementController.JumpMovePosition(currentPosition,
                            _player.DeltaPos, _player.MoveTime, EaseType.QuadraticInOut);
                        break;
                    case Direction.right:
                        currentPosition += Vector3.back;
                        await _movementController.JumpMovePosition(currentPosition,
                            _player.DeltaPos, _player.MoveTime, EaseType.QuadraticInOut);
                        break;
                }


                if (_movementController.TargetPosition.z > _player.DeltaPos * _player.StepCount / 2
                    || _movementController.TargetPosition.z < -_player.DeltaPos * _player.StepCount / 2)
                {
                    await _movementController.Falling(4, _player.MoveTime * 2, EaseType.Linear);
                }
            }
            catch { }
        }

        private void Dispose()
        {
            _inputController.OnSwipe -= InputController_OnSwipe;
            _inputController.OnTouch -= InputController_OnTouch;
        }

        private void Die(bool value)
        {
            if (value)
            {
                Dispose();
            }
            else
            {
                _movementController.OnFall -= Die;
                _player.OnCollision -= Die;
            }
            OnDying?.Invoke(value);
        }
    }
}
