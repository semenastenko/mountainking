﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace Utility
{
    public class BinaryFormatterUtility
    {
        public static string ToBinary(object data)
        {
            using (var memoryStream = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();

                bf.Serialize(memoryStream, data);
                return Convert.ToBase64String(memoryStream.ToArray());
            }
        }

        public static Data FromBinary<Data>(string data)
        {
            var byteData = Convert.FromBase64String(data);
            using (var memoryStream = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();

                memoryStream.Write(byteData, 0, byteData.Length);
                memoryStream.Seek(0, SeekOrigin.Begin);

                return (Data)bf.Deserialize(memoryStream);
            }
        }
    }
}
